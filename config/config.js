/**
 * @link https://moduscreate.com/blog/protractor_parameters_adding_flexibility_automation_tests/
 */
exports.config = {
    params: {
        login: {
            email: 'Chiendiep2013@gmail.com',
            password: '0989092938'
        },
        action: null,
        x:{}
    },
    directConnect: true,
    capabilities: {
        'browserName': (process.env.TEST_BROWSER_NAME || 'chrome'),
        'version': (process.env.TEST_BROWSER_VERSION || 'ANY')
    },
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    specs: [
        './src/features/*.feature'
    ],
    suites: {
        login: ['../src/features/login.feature'],
        youtube: ['../src/features/youtube.feature'],
        register: ['../src/features/register.feature']
    },
    resultJsonOutputFile: './reports/report.json',
    allScriptsTimeout: 50000,
    getPageTimeout: 50000,
    setDefaultTimeout: 60 * 1000,
    onPrepare: function () {
        browser.ignoreSynchronization = true;
        browser.manage().window().maximize();
        var chai = require('chai');
        var chaiAsPromised = require('chai-as-promised');
        chai.use(chaiAsPromised);
        global.expect = chai.expect;
    },
    cucumberOpts: {
        monochrome: true,
        strict: true,
        plugin: ["pretty"],
        require: ['../support/*.js', '../src/stepDefinitions/*.js']
    }
};