Feature: To register a new Google account

  Background:
    Given I am on register page

  Scenario: Verify GUI change plan settings screen
    When I click on first-name field and input my first-name
    And I click on sur-name field and input my sur-name
    And I input my username
    And I input my password
    And I confirm my password
    And I input my birthday
    And I select my gender
    And I input my curent email address
    And I click "next step" button
    And I confirm "terms and conditions"
    And I click submit button
    Then I am on security checking page