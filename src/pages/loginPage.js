var loginPage = function () {
    "use strict";
    var objPage = {
        submitButton: element(by.css("#submitbutton"))
    };

    this.checkBrowserTitle = function () {
        return expect(browser.getTitle()).to.eventually.equal('Youtube');
    };

};
module.exports = loginPage;