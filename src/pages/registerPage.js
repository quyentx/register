var registerPage = function () {
    "use strict";
    var homePage = {
        firstName: element(by.id("FirstName")),
        surName: element(by.id("LastName")),
        address: element(by.id("GmailAddress")),
        password: element(by.id("Passwd")),
        confirmPassword: element(by.id("PasswdAgain")),
        monthSelectBox: element(by.id("BirthMonth")),
        selectedMonth: element(by.id(":3")),
        dateField: element(by.id("BirthDay")),
        yearField: element(by.id("BirthYear")),
        genderField: element(by.id("Gender")),
        genderValue: element(by.id(":e")),
        recoveryMail: element(by.id("RecoveryEmailAddress")),
        nextStepButton: element(by.id("submitbutton")),
        scrollDownButton: element(by.id("tos-scroll-button")),
        saveButton: element(by.css("#iagreebutton")),
        submitButton: element(by.css("#submitbutton"))
    };

    function generateRandomString(len) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < len; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }

    this.checkBrowserTitle = function () {
        return expect(browser.getTitle()).to.eventually.equal('Create your Google Account');
    };

    this.inputFirstname = function () {
        homePage.firstName.sendKeys(generateRandomString(10));
    };

    this.inputSurName = function () {
        homePage.surName.sendKeys(generateRandomString(6));
    };

    this.inputAddress = function () {
        homePage.address.sendKeys(generateRandomString(30));
    };

    this.inputPassword = function () {
        homePage.password.sendKeys("qwqwqtttv4343");
    };

    this.confirmPassword = function () {
        homePage.confirmPassword.sendKeys("qwqwqtttv4343");
    };

    this.selectMonth = function () {
        homePage.monthSelectBox.click();
        homePage.selectedMonth.click();
    };

    this.inputDate = function () {
        homePage.dateField.sendKeys("20");
    };

    this.inputYear = function () {
        homePage.yearField.sendKeys("2004")
    };

    this.selectGender = function () {
        homePage.genderField.click();
        homePage.genderValue.click();
    };

    this.inputEmail = function () {
        homePage.recoveryMail.sendKeys(generateRandomString(8) + "@mail.com");
    };

    this.clickNextStepButton = function () {
        homePage.nextStepButton.click();
    };

    this.scrollDown = function () {
        homePage.scrollDownButton.click();
        homePage.scrollDownButton.click();
        homePage.scrollDownButton.click();
        homePage.scrollDownButton.click();
        homePage.scrollDownButton.click();
    };

    this.clickAgreebtn = function () {
        browser.sleep(5000);
        homePage.saveButton.click();
    };

    this.clickSubmitButton = function () {
        homePage.submitButton.click();
    };

    this.checkSecurityPage = function () {
//        return(expect(browser.getCurrentUrl).toBe("https://myaccount.google.com/u/1/security"))
        return expect(browser.getCurrentUrl()).to.eventually.equal('https://accounts.google.com/UserSignUpIdvChallenge');
    }

};
module.exports = registerPage;