var request = require('request');
var youtubePage = function () {
    "use strict";
    var objPage = {
        loginButton: element(by.css("#buttons > ytd-button-renderer.style-scope.ytd-masthead.style-brand > a")),
        emailInput: element(by.id("identifierId")),
        nextToPasswordButton: element(by.id("identifierNext")),
        passwordInput: element(by.name('password')),
        passwordNextButton: element(by.id('passwordNext')),
        /* Youtube */
        avatarButton: element(by.id('avatar-btn')),
        linkMyChannel: element(by.css('#label.ytd-compact-link-renderer')),
    };

    var createChannel = {
        dialogTitle: element(by.id('yt-dialog-title-2')),
        firstName: element(by.id('create-channel-first-name')),
        lastName: element(by.id('create-channel-last-name')),
    };

    this.checkBrowserTitle = function () {
        return expect(browser.getTitle()).to.eventually.equal('YouTube');
    };

    this.clickLoginButton = function () {
        objPage.loginButton.click();
    };

    this.emailInput = function () {
        objPage.emailInput.sendKeys(browser.params.login.email);
    };

    this.clickNextToPasswordButton = function () {
        objPage.nextToPasswordButton.click();
    };

    this.passwordInput = function () {
        browser.sleep(1000);
        objPage.passwordInput.sendKeys(browser.params.login.password);
    };

    this.clickNextButtonToLogin = function () {
        objPage.passwordNextButton.click();
    };

    this.clickToAvatarButton = function () {
        browser.sleep(1000);
        objPage.avatarButton.click();
    };

    this.clickToCreateNewChannel = function () {
        browser.sleep(1000);
        browser.get('https://youtube.com/create_channel');
    };

    this.checkPageTitleOfCreateChannelPage = function () {
        expect(browser.getTitle()).to.eventually.equal('YouTube');
    };

    this.checkDialogTitleAtCreateChannelPage = function () {
        browser.ignoreSynchronization = true;
        browser.sleep(3000);
        request('http://corollamedia.com/api/get-name.json', function (error, response, body) {
            var b = JSON.parse(body);

            createChannel.firstName.sendKeys(b.first_name);
            createChannel.lastName.sendKeys(b.last_name);
        });


//        createChannel.firstName.sendKeys(b.first_name);
//        createChannel.lastName.sendKeys(b.last_name);

//        createChannel.firstName.sendKeys("1111");

//        console.info("================");
//        console.info(createChannel.dialogTitle.getText());
//        console.info("================");
////        expect(createChannel.dialogTitle.getText()).equal('Use YouTube as...');
    };
};
module.exports = youtubePage;