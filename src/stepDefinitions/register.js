var homepage = require('../pages/registerPage');
var register = function () {
	"use strict";
	var homeObj = new homepage();

    this.setDefaultTimeout(60 * 1000);

    this.Given(/^I am on register page$/, function(){
        homeObj.checkBrowserTitle();
    });

    this.Then(/^I click on first-name field and input my first-name$/, function(){
    	homeObj.inputFirstname();
    });

    this.Then(/^I click on sur-name field and input my sur-name$/, function(){
    	homeObj.inputSurName();
    });

    this.Then(/^I input my username$/, function(){
    	homeObj.inputAddress();
    });

    this.Then(/^I input my password$/, function(){
    	homeObj.inputPassword();
    });

    this.Then(/^I confirm my password$/, function(){
    	homeObj.confirmPassword();
    });

    this.Then(/^I input my birthday$/, function(){
        homeObj.selectMonth();
        homeObj.inputDate();
        homeObj.inputYear();
	});

    this.Then(/^I select my gender$/, function(){
        homeObj.selectGender();
    });

    this.Then(/^I click "next step" button$/, function () {
        homeObj.clickNextStepButton();
    });

    this.Then(/^I input my curent email address$/, function(){
        homeObj.inputEmail();
    });

    this.Then(/^I confirm "terms and conditions"$/, function(){
        homeObj.scrollDown();
        homeObj.clickAgreebtn();
    });

    this.Then(/^I click submit button$/, function(){
        homeObj.clickSubmitButton();
    });

    this.Then(/^I am on security checking page$/, function(){
        homeObj.checkSecurityPage();
    });
};
module.exports = register;