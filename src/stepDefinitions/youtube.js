var homepage = require('../pages/youtubePage');
var youtube = function () {
    "use strict";
    var pageObj = new homepage();

    this.setDefaultTimeout(60 * 1000);

    this.Given(/^I am on YouTube page$/, function(){
        pageObj.checkBrowserTitle();
    });

    this.Then(/^I click to sigin button at top right$/, function () {
        pageObj.clickLoginButton();
    });
    
    this.Then(/^I writing email address$/, function(){
    	pageObj.emailInput();
    });
    
    this.Then(/^I click to next button to writing password$/, function(){
    	pageObj.clickNextToPasswordButton();
    });
    
    this.Then(/^I writing password$/, function(){
    	pageObj.passwordInput();
    });
    
    this.Then(/^I click to Login button$/, function(){
    	pageObj.clickNextButtonToLogin();
    });
    
    this.Then(/^I am click to avatar button at top right$/, function(){
    	pageObj.clickToAvatarButton();
    });
    
    this.Then(/^I am click to create new channel$/, function(){
    	pageObj.clickToCreateNewChannel();
    });
    
    this.Then(/^I am check page title at create channel page$/, function(){
    	pageObj.checkPageTitleOfCreateChannelPage();
    });
    
    this.Then(/^I am check create channel dialog title$/, function(){
    	pageObj.checkDialogTitleAtCreateChannelPage();
    });
    
};
module.exports = youtube;