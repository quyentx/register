/*jslint node: true*/
var Cucumber = require('cucumber');
var fs = require('fs');
var conf = require('../config/config').config;

var hooks = function () {
    "use strict";
    var outputDir = './reports/';

    this.registerHandler('BeforeScenario', function () {
        var url = null;
        switch (browser.params.action) {
            case 'register':
            default:
                url = 'https://accounts.google.com/SignUp';
                break;
            case 'login':
                url = 'https://accounts.google.com/Login';
                break;
            case 'youtube':
                url = 'https://www.youtube.com/';
                break;
        }
        return browser.get(url);
    });

    var createHtmlReport = function () {

        // generate bootstrap_report.html from report.json
        var reporter = require('cucumber-html-reporter');
        var options = {
            theme: 'bootstrap',
            jsonFile: 'reports/report.json',
            output: 'reports/bootstrap_report.html',
            reportSuiteAsScenarios: true
        };

        reporter.generate(options);
    };

    var JsonFormatter = Cucumber.Listener.JsonFormatter();
    JsonFormatter.log = function (string) {
        if (!fs.existsSync(outputDir)) {
            fs.mkdirSync(outputDir);
        }

        var targetJson = outputDir + 'report.json';
        fs.writeFile(targetJson, string, function (error) {
            if (error) {
                console.log('Failed to save cucumber test results to json file.');
                console.log(error);
            } else {
                createHtmlReport(targetJson);
            }
        });
    };

    this.registerListener(JsonFormatter);

};
module.exports = hooks;